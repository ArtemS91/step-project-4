
class Modal {
    constructor(cardiologist,dentist,therapeutic){
    this.cardiologist= cardiologist;
    this.dentist = dentist;
    this.therapeutic =therapeutic
}
    createModal(){
        let modal = `
    <section class="create-modal">
    <div class="creating__visit">
        <div class="modal-login__close-btn">&times;</div>
              <h3 class="modal-login__title">Створити Візит</h3>  
              <select class="choose__doctor">
                <option value="Виберіть доктора">Виберіть доктора</option>
                <option value="Сardiologist">${this.cardiologist}</option>
                <option value="Dentist">${this.dentist}</option>
                <option value="Therapeutic">${this.therapeutic}</option>   
             </select>  
                <form class="modal-creating__form form flex__forma">            
                  
                </form>
        <button class="btn btn__send__visit">Відправити</button>
    </div>
    </section>
        `
    document.body.insertAdjacentHTML('beforeend', modal);
    }
}

class Visit {
    constructor(fullName,purpose,description,impotent){
        this.fullName = fullName;
        this.purpose =purpose;
        this.description =description;
        this.impotent=impotent
    }
createForma(elem){
    let addForm= `
                <label for="fullName"class="visit__labels">${this.fullName}:</label>
                     <input name="fullName" type="text" class="border">
               <label for="purpose" class="visit__labels">${this.purpose}:</label>
                     <input name="purpose" type="text" class="border">
                <label class="visit__labels">${this.description}:</label>
                     <textarea cols="10" id="" name="опис візиту" rows="5"></textarea>
                <label for="impotent" class="visit__labels">${this.impotent}:</label>
                <select name="impotent">
                    <option value="Усі">All</option>
                    <option value="Невідкладна">Невідкладна</option>
                    <option value="Пріоритетна">Пріоритетна</option>
                    <option value="Звичайна">Звичайна</option>                
              </select>`
            elem.insertAdjacentHTML("beforeend",addForm)
}
}
class VisitСardiologist extends Visit{
    constructor(initials,purpose,description,impotent, pressure,indexWeight,heartDisease,age){
        super(initials,purpose,description,impotent);
        this.pressure = pressure;
        this.indexWeight =indexWeight;
        this.heartDisease =heartDisease;
        this.age =age
    }
    createFormСardiologist(elem){
        super.createForma(elem)
        let addForm =`
        <div class="add__inputs Сardiologist">
           <label class="visit__labels">${this.pressure}:</label>
                <input type="text" class="border">
           <label class="visit__labels">${this.indexWeight}:</label>
                <input type="text" class="border">
           <label class="visit__labels">${this.heartDisease}:</label>
                <input type="text" class="border">
           <label for="age"class="visit__labels">${this.age}:</label>
                <input name="age"type="text" class="border">
       </div>
        `
        elem.insertAdjacentHTML("beforeend", addForm)
    }
}
class VisitDentist extends Visit{
    constructor(initials,purpose,description,impotent,date){
        super(initials,purpose,description,impotent);
        this.date =date
    }
    createFormDentist(elem){
        super.createForma(elem)
        let addForm =`
        <div class="add__inputs Dentist">
           <label for="data-visit" class="visit__labels">${this.date}:</label>
           <input type="date" name="data-visit" class="border">
       </div>
        `
        elem.insertAdjacentHTML("beforeend", addForm)
    }
}
class VisitTherapeutic extends Visit{
    constructor(initials,purpose,description,impotent,age){
        super(initials,purpose,description,impotent);
        this.age =age
    }
    createFormTherapeutic(elem){
        super.createForma(elem)
        let addForm =`
        <div class="add__inputs Therapeutic">
           <label for="age" class="visit__labels">${this.age}:</label>
           <input type="text" name="age" class="border">
       </div>
        `
        elem.insertAdjacentHTML("beforeend", addForm)
    }
}
let modal = new Modal("Кардіолог","Стоматолог","Терапевт");
let visitСardiologist = new VisitСardiologist("ПІБ","Мета візиту","Короткий опис візиту","Tерміновість","Звичайний тиск","Індекс маси тіла","Перенесені захворювання серцево-судинної системи","Вік")
let visitDentist = new VisitDentist("ПІБ","Мета візиту","Короткий опис візиту","Tерміновість","Дата останнього відвідування")
let visitTherapeutic = new VisitTherapeutic("ПІБ","Мета візиту","Короткий опис візиту","Tерміновість","Вік")
const createVisitButton = document.querySelector('.header__btn-create-visit');
createVisitButton.addEventListener("click",(e)=>{
    e.preventDefault();
    modal.createModal()
    const form =document.querySelector(".modal-creating__form");
    const chooseDoctor = document.querySelector(".choose__doctor");
    const doctors = {};
    function makeDoctorsObject(key,value,modal){
        return doctors[key]= value.bind(modal)
    }
    makeDoctorsObject("Сardiologist",visitСardiologist.createFormСardiologist,visitСardiologist)
    makeDoctorsObject("Dentist",visitDentist.createFormDentist,visitDentist)
    makeDoctorsObject("Therapeutic",visitTherapeutic.createFormTherapeutic,visitTherapeutic)
    chooseDoctor.addEventListener("change", ()=>{
        form.innerHTML="";////delete form content
        doctors[chooseDoctor.value](form);
    })
    })
   



