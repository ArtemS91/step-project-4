import Modal from './_modal.js'
import {toggleClass} from "../app.js"
import {renderingLoginCards} from "./_creatCard.js"

const openModalBtn = document.querySelector('.header__btn-login')
let modalLogin = null
// Creating an empty variable 'token'
let token = ''
const loginInputsHtml = `
     <label for="email">Email:</label>
     <input class="modal__email" id="email" name="email" type="text" data-input>
     
     <label for="password">Пароль:</label>
     <input class="modal__password" id="password" name="password" type="password" data-input>
`

const fetchRequest = async (target) => {
    try {
        token = await target.handleSubmitForm("https://ajax.test-danit.com/api/v2/cards/login",
            'POST',
            {'Content-Type': 'application/json'},
            'text')
        localStorage.setItem('token', JSON.stringify(token))
        target.closeModal()
        toggleClass('header__btn')
        toggleClass('board-of-cards__text')
        renderingLoginCards(token)
    } catch (err) {
        console.log(err.text)
        fetchRequest(target)
    }
}

openModalBtn.addEventListener('click', () => {
    modalLogin = new Modal('Вхід на сайт', 'Увійти')
    modalLogin.setInputs(loginInputsHtml)
    modalLogin.handleCloseModal()
    fetchRequest(modalLogin)
})

function reloadPage(localToken) {
    if (localToken) {
        token = localToken
        toggleClass('header__btn')
        toggleClass('board-of-cards__text')
        renderingLoginCards(localToken)
    }
}

window.onload = () => {
    reloadPage(JSON.parse(localStorage.getItem('token')))
}


export {token}

